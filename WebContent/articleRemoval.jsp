<%@page import="java.io.File"%>
<%@page import="java.time.LocalDateTime"%>
<%@page import="java.util.Map"%>
<%@page import="com.board2.Article"%>
<%@page import="com.board2.BoardService"%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%
	Map<String, String[]> reqMap = request.getParameterMap();
	Article article = null;
	if (reqMap.containsKey("article_id")) {
		BoardService boardService = new BoardService();
		try {
			int id = Integer.parseInt(request.getParameter("article_id"));
			article = boardService.getArticle(id);
			int affectedRow = boardService.remove(id);
			if (affectedRow > 0) {

				File f = new File(request.getServletContext().getRealPath("") + File.separator + "uploadFiles" + File.separator + article.getFileName());
				if (f.exists()) {
					f.delete();
				}
			}
			response.sendRedirect("index.jsp");
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
	} else {
		response.getOutputStream().println("파라미터 예외발생");
	}
	// Article article = new Article();
	// article.title = "제목";
	// article.author = "작성자";
	// article.content = "내용";
	// BoardService boardService = new BoardService();
%>