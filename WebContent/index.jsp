<%@page import="java.time.format.DateTimeFormatter"%>
<%@page import="com.board2.Article"%>
<%@page import="java.util.List"%>
<%@page import="com.board2.Page"%>
<%@page import="com.board2.BoardService"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<style>
ul {
	list-style: none;
}

li {
	display: inline-block;
}
</style>
</head>
<body>
	<%
		BoardService boardService = new BoardService();
		int articlesPerPage = boardService.getIntParam(request, "articlesPerPage");
		int currentPage = boardService.getIntParam(request, "currentPage");
		boolean changeArticlesPerPage = boardService.getBooleanParam(request, "changeArticlesPerPage");
			
		Page pageM = boardService.getPage();
		pageM.setPage(currentPage, articlesPerPage, changeArticlesPerPage);
		
		List<Article> articles = boardService.getArticles(pageM);
	%>
	<h1>게시판</h1>
	<a href="edit.jsp">글쓰기</a>
	<ul>
		<li><a
			href="index.jsp?currentPage=<%=pageM.getCurrentPage() - 1%>&articlesPerPage=<%=pageM.getArticlesPerPage()%>">이전</a></li>
		<%
			for (int i = 0; i < pageM.getMaxPage(); i++) {
		%>
		<li><a href="index.jsp?currentPage=<%=i + 1%>&articlesPerPage=<%=pageM.getArticlesPerPage()%>"><%=i + 1%></a></li>
		<%
			}
		%>
		<li><a
			href="index.jsp?currentPage=<%=pageM.getCurrentPage() + 1%>&articlesPerPage=<%=pageM.getArticlesPerPage()%>">다음</a></li>
	</ul>
	<select>
		<%
			for (Page.ArticlesPerPage volume : Page.ArticlesPerPage.values()) {
		%>
		<option <%= (pageM.getArticlesPerPage() == volume.getValue()) ? "selected" : "" %>><%=volume.getValue() %></option>
		<%
			}
		%>
	</select>
	<table class="board" data-current-page="<%=pageM.getCurrentPage() %>" data-articles-per-page="<%=pageM.getArticlesPerPage()%>">
		<tr>
			<th>id</th>
			<th>제목</th>
			<th>작성자</th>
			<th>작성일자</th>
		</tr>
		<%
			for (Article article : articles) {
		%>
		<tr class="article" data-article-id="<%=article.getId()%>">
			<td><%=article.getId()%></td>
			<td><%=article.getTitle()%></td>
			<td><%=article.getAuthor()%></td>
			<td><%=article.getAddedAt().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"))%></td>
		</tr>
		<%
			}
		%>
	</table>
	<script>
		document.querySelector(".board").addEventListener(
				"click",
				function(e) {
					location.href = "article.jsp?article_id="
							+ e.target.closest(".article").dataset.articleId;
				}, false);
		document.querySelector("select").addEventListener("change",	function(e){
			var board = document.querySelector(".board");
			location.href = "index.jsp?currentPage="+ board.dataset.currentPage+"&articlesPerPage="+e.target.value+"&changeArticlesPerPage="+(e.target.value != board.dataset.articlesPerPage);
		}, false);
	</script>
</body>
</html>