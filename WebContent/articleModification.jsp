<%@page import="java.util.HashMap"%>
<%@page
	import="org.apache.tomcat.util.http.fileupload.servlet.ServletRequestContext"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.apache.tomcat.util.http.fileupload.FileItem"%>
<%@page import="java.util.List"%>
<%@page
	import="org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload"%>
<%@page
	import="org.apache.tomcat.util.http.fileupload.disk.DiskFileItemFactory"%>
<%@page import="java.io.File"%>
<%@page import="java.time.format.DateTimeFormatter"%>
<%@page import="java.time.LocalDateTime"%>
<%@page import="java.util.Map"%>
<%@page import="com.board2.Article"%>
<%@page import="com.board2.BoardService"%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%
	if (!ServletFileUpload.isMultipartContent(request)) {
		throw new Exception();
	}
	Map<String, String> paramMap = new HashMap<String, String>();
	BoardService boardService = new BoardService();
	String fileName = null;
	boolean modified = true;
	String[] strs = { "article_id", "title", "author", "content" };
	for (String str : strs) {
		paramMap.put(str, null);
	}

	File file;
	String saveDir = "uploadFiles";
	String appPath = request.getServletContext().getRealPath("");
	String savePath = appPath + File.separator + saveDir;
	DiskFileItemFactory factory = new DiskFileItemFactory();

	factory.setRepository(new File("c://temp"));

	//Create a new file upload handler
	ServletFileUpload upload = new ServletFileUpload(factory);

	//Parse the request
	List<FileItem> items = upload.parseRequest((new ServletRequestContext(request)));

	Iterator<FileItem> iter = items.iterator();
	LocalDateTime now = LocalDateTime.now();
	while (iter.hasNext()) {
		FileItem item = iter.next();

		String fieldName = item.getFieldName();

		if (item.isFormField() && paramMap.keySet().contains(fieldName)) { // processFormField
			String value = item.getString("UTF-8");
			paramMap.put(fieldName, value);

		} else if (!item.isFormField() && item.getSize() > 0) {
			fileName = item.getName();
			fileName = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddhhmmssnnnnnnnnn")) + "_"
					+ fileName;
			file = new File(savePath + File.separator + fileName);
			item.write(file);
			modified = true;
		}
	}
	int id = Integer.parseInt(paramMap.get("article_id"));
	Article article = boardService.getArticle(id);
	article.setTitle(paramMap.get("title"));
	article.setAuthor(paramMap.get("author"));
	article.setContent(paramMap.get("content"));
	article.setAddedAt(article.getAddedAt());
	fileName = (modified) ? fileName : article.getFileName();
	if (modified && (null != article.getFileName())) {
		File f = new File(savePath + File.separator + article.getFileName());
		if (f.exists()) {
			f.delete();
		}
	}
	article.setFileName(fileName);
	boardService.modify(article);
	response.sendRedirect("index.jsp");
%>