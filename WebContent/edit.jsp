<%@page import="org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload"%>
<%@page import="com.board2.BoardService"%>
<%@page import="com.board2.Article"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%
int id = 0;
Article article = null;
if (request.getParameterMap().containsKey("article_id")) {
	try {
		id = Integer.parseInt(request.getParameter("article_id"));
		BoardService boardService = new BoardService();
		article = boardService.getArticle(id);
	} catch (NumberFormatException e) {
		e.printStackTrace();
	}
}

%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<h1><%= (0 < id) ? "글수정" : "글쓰기"%></h1>
<form action="<%= (0 < id) ? "articleModification.jsp" : "articleRegistration.jsp"%>" Method="POST" enctype="multipart/form-data">
<input name="article_id" type="hidden" value="<%= (null == article) ? "" : article.getId()%>">
제목<input name="title" type="text" value="<%= (null == article) ? "" : article.getTitle()%>">
작성자<input name="author" type="text" value="<%= (null == article) ? "" : article.getAuthor()%>">
내용<input name="content" type="text" value="<%= (null == article) ? "" : article.getContent()%>">
파일<input name="uploadFile" type="file"/>
<input type="submit" value="<%= (0 < id) ? "수정" : "등록"%>">
</form>
</body>
</html> 