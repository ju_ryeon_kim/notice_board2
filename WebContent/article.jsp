<%@page import="com.board2.BoardService"%>
<%@page import="com.board2.Article"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%
	int id = 0;
	Article article = null;
	if (request.getParameterMap().containsKey("article_id")) {
		try {
			id = Integer.parseInt(request.getParameter("article_id"));
			BoardService boardService = new BoardService();
			article = boardService.getArticle(id);
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
	}
%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<h1>글읽기</h1>
	제목
	<input name="title" type="text" disabled
		value="<%=article.getTitle()%>"> 작성자
	<input name="author" type="text" disabled
		value="<%=article.getAuthor()%>"> 내용
	<input name="content" type="text" disabled
		value="<%=article.getContent()%>">
	<%
		if (null == article.getFileName()) {
	%>
	첨부파일 없음
	<%
		} else {
	%>
	첨부파일 <a href="downloadFile.jsp?article_id=<%=article.getId()%>"><%= article.getFileName().substring(article.getFileName().indexOf("_")+1) %></a>
	<%
		}
	%>
	<a></a>
	<a href="edit.jsp?article_id=<%=id%>">수정</a>
	<a href="articleRemoval.jsp?article_id=<%=id%>">삭제</a>
</body>
</html>
