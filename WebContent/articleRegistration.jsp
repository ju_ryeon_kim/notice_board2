<%@page import="java.time.format.DateTimeFormatter"%>
<%@page
	import="org.apache.tomcat.util.http.fileupload.servlet.ServletRequestContext"%>
<%@page import="org.apache.tomcat.util.http.fileupload.RequestContext"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.apache.tomcat.util.http.fileupload.FileItem"%>
<%@page import="java.util.List"%>
<%@page
	import="org.apache.tomcat.util.http.fileupload.disk.DiskFileItemFactory"%>
<%@page import="java.util.HashMap"%>
<%@page
	import="org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload"%>
<%@page import="java.io.FileOutputStream"%>
<%@page import="java.io.InputStream"%>
<%@page import="java.util.Enumeration"%>
<%@page import="java.io.File"%>
<%@page import="java.time.LocalDateTime"%>
<%@page import="java.util.Map"%>
<%@page import="com.board2.Article"%>
<%@page import="com.board2.BoardService"%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%
	if (!ServletFileUpload.isMultipartContent(request)) {
		throw new Exception();
	}
	Map<String, String> paramMap = new HashMap<String, String>();
	BoardService boardService = new BoardService();
	String fileName = null;
	String[] strs = { "title", "author", "content" };
	for (String str : strs) {
		paramMap.put(str, null);
	}

	File file;
	String saveDir = "uploadFiles";
	String appPath = request.getServletContext().getRealPath("");
	String savePath = appPath + File.separator + saveDir;
	DiskFileItemFactory factory = new DiskFileItemFactory();

	factory.setRepository(new File("c://temp"));

	//Create a new file upload handler
	ServletFileUpload upload = new ServletFileUpload(factory);

	//Parse the request
	List<FileItem> items = upload.parseRequest((new ServletRequestContext(request)));

	Iterator<FileItem> iter = items.iterator();
	LocalDateTime now = LocalDateTime.now();
	while (iter.hasNext()) {
		FileItem item = iter.next();

		String fieldName = item.getFieldName();

		if (item.isFormField() && paramMap.keySet().contains(fieldName)) { // processFormField
			String value = item.getString("UTF-8");
			paramMap.put(fieldName, value);

		} else if (!item.isFormField() && item.getSize() > 0 ) {
			fileName = item.getName();
			fileName = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddhhmmssnnnnnnnnn")) + "_" + fileName;
			System.out.println(LocalDateTime.now().getNano());
			file = new File(savePath + File.separator + fileName);
			item.write(file);
		}
	}
	Article article = null;
	article = new Article();
	article.setTitle(paramMap.get("title"));
	article.setAuthor(paramMap.get("author"));
	article.setContent(paramMap.get("content"));
	article.setAddedAt(LocalDateTime.now());
	article.setFileName(fileName);
	boardService.add(article);
	response.sendRedirect("index.jsp");
%>