<%@page import="java.io.IOException"%>
<%@page import="java.net.URLEncoder"%>
<%@page import="java.io.FileInputStream"%><%@page import="java.io.File"%><%@page
	import="com.board2.Article"%><%@page import="com.board2.BoardService"%><%@ 
page
	language="java" pageEncoding="UTF-8"%>
<%
	ServletContext context = request.getServletContext();

	BoardService boardService = new BoardService();
	int id = boardService.getIntParam(request, "article_id");
	Article article = boardService.getArticle(id);
	String fileName = article.getFileName();
	String saveDir = "uploadFiles";
	String appPath = context.getRealPath("");
	String savePath = appPath + File.separator + saveDir;
	String filePath = savePath + File.separator + fileName;
	File downloadFile = new File(filePath);

	String mimeType = context.getMimeType(filePath);
	response.setContentType(mimeType != null ? mimeType : "application/octet-stream");
	response.setContentLength((int) downloadFile.length());
	String headerKey = "Content-Disposition";
	String headerValue = String.format("attachment; filename=\"%s\"",
			URLEncoder.encode(fileName.substring(fileName.indexOf("_") + 1), "UTF-8"));
	response.setHeader(headerKey, headerValue);

	out.clear();
	pageContext.pushBody();
	ServletOutputStream os = null;
	FileInputStream fis = null;
	try {
		fis = new FileInputStream(downloadFile);
		os = response.getOutputStream();
		byte[] bufferData = new byte[(int) downloadFile.length()];
		int read = 0;
		while ((read = fis.read(bufferData)) != -1) {
			os.write(bufferData, 0, read);
		}
	} finally {
		os.flush();
		os.close();
		fis.close();
	}
%>