package com.board2;

import static org.junit.Assert.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.List;

import org.junit.Test;

public class FunctionTest2 {



	@Test
	public void 디비서비스_초기화() throws Exception {
		// Given

		// when
		Connection conn = DBService.getConnection();
		assertNotNull(conn);

		// then
	}

	@Test
	public void 디비서비스_커넥션프로세스() throws SQLException {
		// Given

		// when
		Connection conn = null;
		PreparedStatement preStmt = null;
		ResultSet rs = null;
		int result = 0;
		try {
			conn = DBService.getConnection();
			String sql = "SELECT * FROM articles";
			preStmt = conn.prepareStatement(sql);
			rs = preStmt.executeQuery();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DBService.disConnection(conn, preStmt, rs);
		}
		// then
		assertTrue(rs.isClosed());
		assertTrue(preStmt.isClosed());
		assertTrue(conn.isClosed());
	}

	@Test
	public void 게시글등록하기() throws Exception {
		// Given
		Article article = new Article();
		article.setTitle("제목");
		article.setAuthor("작성자");
		article.setContent("내용");
		article.setAddedAt(LocalDateTime.now());
		BoardService boardService = new BoardService();

		// when
		int id = boardService.add(article);
		// then
		assertTrue(0 < id);
	}
	
	@Test
	public void 게시글삭제하기() throws Exception {
		//Given
		BoardService boardService = new BoardService();
		Article article = new Article();
		article.setTitle("제목");
		article.setAuthor("작성자");
		article.setContent("내용");
		article.setAddedAt(LocalDateTime.now());
		int id = boardService.add(article);
		//when
		int affectedRow = boardService.remove(id);
		//then
		assertTrue(0 < affectedRow);
	}
	
	@Test
	public void 페이지정보_초기화_전체게시글수및전체페이지수() throws Exception {
		//Given
		BoardService boardService = new BoardService();
		Page page = boardService.getPage();
		//when
		int maxPage = (int) Math.ceil(boardService.getTotalArticleCount() / (double)page.getArticlesPerPage());
		//then
		assertEquals(maxPage, page.getMaxPage());
	}
	
	@Test
	public void 페이지정보_페이지당게시글수변경시_총페이지수변경() throws Exception {
		//Given
		BoardService boardService = new BoardService();
		Page page = boardService.getPage();

		//when
		page.setArticlesPerPage(1);

		//then
		assertEquals(boardService.getTotalArticleCount(), page.getMaxPage());
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void 페이지정보_페이지당게시글수변경시_예외값() throws Exception {
		//Given
		BoardService boardService = new BoardService();
		Page page = boardService.getPage();

		//when
		page.setArticlesPerPage(0);
		page.setArticlesPerPage(-1);

		//then
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void 페이지정보_현재페이지변경시_예외값() throws Exception {
		//Given
		BoardService boardService = new BoardService();
		Page page = boardService.getPage();

		//when
		page.setCurrentPage(0);
		page.setCurrentPage(-1);
		page.setCurrentPage(page.getMaxPage() + 1);

		//then
	}
	
	@Test
	public void 페이지정보_페이지당게시글수변경시_현재페이지변경() throws Exception {
		//Given
		BoardService boardService = new BoardService();
		Page page = boardService.getPage();

		//when
		page.setArticlesPerPage(1);
		page.setCurrentPage(page.getMaxPage());
		page.setArticlesPerPage(2);
	
		//then
		assertEquals(1, page.getCurrentPage());
	}
	
	@Test
	public void 게시글_현재페이지게시글얻기() throws Exception {
		//Given
		BoardService boardService = new BoardService();
		Page page = boardService.getPage();
		page.setArticlesPerPage(boardService.getTotalArticleCount());
		//when
		List<Article> articles = boardService.getArticles(page);
		//then
		assertEquals(boardService.getTotalArticleCount(), articles.size());
	}
	
	@Test
	public void 아이디값으로_게시글얻기() throws Exception {
		//Given
		BoardService boardService = new BoardService();
		Article article = new Article();
		article.setTitle("제목");
		article.setAuthor("작성자");
		article.setContent("내용");
		article.setAddedAt(LocalDateTime.now());
		int id = boardService.add(article);
		//when
		Article article2 = boardService.getArticle(id);
		//then
		assertEquals(id, article2.getId());
	}
	
	@Test
	public void 게시글수정() throws Exception {
		//Given
		BoardService boardService = new BoardService();
		Article article = new Article();
		article.setTitle("제목");
		article.setAuthor("작성자");
		article.setContent("내용");
		article.setAddedAt(LocalDateTime.now());
		int id = boardService.add(article);
		//when
		Article article2 = boardService.getArticle(id);
		article2.setTitle("타이틀");
		boardService.modify(article2);
		Article article3 = boardService.getArticle(id);
		//then
		assertEquals(article2.getId(), article3.getId());
		assertEquals("타이틀", article3.getTitle());
	}
	
	// @Test
	// public void test1() {
	// BoardService boardService = new BoardService();
	// Page page = boardService.getPage();
	// assertEquals(1, page.currentPage);
	// assertEquals(10, page.articlesPerPage);
	// }
	//
	// @Test
	// public void test2() throws Exception {
	// //Given
	// BoardService boardService = new BoardService();
	// Page page = boardService.getPage();
	// page.articlesPerPage = 1;
	// page.currentPage = 10;
	// List<Article> articles = boardService.getArticles(page);
	//
	// //when
	//
	//
	// //then
	//
	// }

}
