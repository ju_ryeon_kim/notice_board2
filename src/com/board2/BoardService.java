package com.board2;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.tomcat.util.http.fileupload.FileItem;
import org.apache.tomcat.util.http.fileupload.FileUploadException;
import org.apache.tomcat.util.http.fileupload.disk.DiskFileItemFactory;
import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;
import org.apache.tomcat.util.http.fileupload.servlet.ServletRequestContext;

public class BoardService {

	public List<Article> getArticles(Page page) {
		Connection conn = null;
		PreparedStatement preStmt = null;
		ResultSet rs = null;
		List<Article> articles = new ArrayList<>();
		try {
			conn = DBService.getConnection();
			String sql = "SELECT * FROM articles ORDER BY article_id DESC LIMIT ?, ?";
			preStmt = conn.prepareStatement(sql);
			preStmt.setObject(1, page.getPageOffset());
			preStmt.setObject(2, page.getArticlesPerPage());
			rs = preStmt.executeQuery();
			while (rs.next()) {
				Article article = new Article();
				article.setId(rs.getInt("article_id"));
				article.setAuthor(rs.getString("author"));
				article.setTitle(rs.getString("title"));
				article.setContent(rs.getString("content"));
				article.setAddedAt(rs.getTimestamp("added_at").toLocalDateTime());
				article.setFileName(rs.getString("fileName"));
				articles.add(article);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DBService.disConnection(conn, preStmt, rs);
		}
		return articles;
	}

	
	public Page getPage() {
		Page page = new Page(getTotalArticleCount());
		return page;
	}
	
	public int getTotalArticleCount() {
		Connection conn = null;
		PreparedStatement preStmt = null;
		ResultSet rs = null;
		int count = 0;
		try {
			conn = DBService.getConnection();
			String sql = "SELECT COUNT(article_id) FROM articles";
			preStmt = conn.prepareStatement(sql);
			rs = preStmt.executeQuery();
			if(rs.next()) {
				count = rs.getInt(1);
			}
		} catch (SQLException e) {
			DBService.disConnection(conn, preStmt, rs);
		}
		return count;
	}

	public int add(Article article) {
		Connection conn = null;
		PreparedStatement preStmt = null;
		ResultSet rs = null;
		int result = 0;
		try {
			conn = DBService.getConnection();
			String sql = "INSERT INTO articles (author, title, content, added_at, filename) VALUES(?,?,?,?,?)";
			preStmt = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			preStmt.setObject(1, article.getAuthor());
			preStmt.setObject(2, article.getTitle());
			preStmt.setObject(3, article.getContent());
			preStmt.setObject(4, Timestamp.valueOf(article.getAddedAt()));
			preStmt.setObject(5, article.getFileName());
			preStmt.executeUpdate();
			rs = preStmt.getGeneratedKeys();
			if (rs.next()) {
				result = rs.getInt(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DBService.disConnection(conn, preStmt, rs);
		}
		return result;
	}

	public int remove(int id) {
		Connection conn = null;
		PreparedStatement preStmt = null;
		int affectedRow = 0;
		try {
			conn = DBService.getConnection();
			String sql = "DELETE FROM articles WHERE article_id = ?";
			preStmt = conn.prepareStatement(sql);
			preStmt.setObject(1, id);
			affectedRow = preStmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DBService.disConnection(conn, preStmt, null);
		}
		return affectedRow;
	}

	public Article getArticle(int id) {
		Connection conn = null;
		PreparedStatement preStmt = null;
		ResultSet rs = null;
		Article article = null;
		try {
			conn = DBService.getConnection();
			String sql = "SELECT * FROM articles WHERE article_id = ?";
			preStmt = conn.prepareStatement(sql);
			preStmt.setObject(1, id);
			rs = preStmt.executeQuery();
			if (rs.next()) {
				article = new Article();
				article.setId(rs.getInt("article_id"));
				article.setAuthor(rs.getString("author"));
				article.setTitle(rs.getString("title"));
				article.setContent(rs.getString("content"));
				article.setAddedAt(rs.getTimestamp("added_at").toLocalDateTime());
				article.setFileName(rs.getString("fileName"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DBService.disConnection(conn, preStmt, rs);
		}
		return article;
	}

	public int modify(Article article) {
		Connection conn = null;
		PreparedStatement preStmt = null;
		ResultSet rs = null;
		int affectedRow = 0;
		try {
			conn = DBService.getConnection();
			String sql = "UPDATE articles SET author=?, title=?, content=?, filename=? WHERE article_id = ?";
			preStmt = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			preStmt.setObject(1, article.getAuthor());
			preStmt.setObject(2, article.getTitle());
			preStmt.setObject(3, article.getContent());
			preStmt.setObject(4, article.getFileName());
			preStmt.setObject(5, article.getId());
			affectedRow = preStmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DBService.disConnection(conn, preStmt, rs);
		}
		return affectedRow;
	}

	public int getIntParam(HttpServletRequest request, String key) throws NumberFormatException {
		int result = -1;
		if(request.getParameterMap().containsKey(key)){
			result = Integer.parseInt(request.getParameter(key));
		}
		return result;
	}
	
	public boolean getBooleanParam(HttpServletRequest request, String key) throws NumberFormatException {
		boolean result = false;
		if(request.getParameterMap().containsKey(key)){
			result = Boolean.parseBoolean(request.getParameter(key));
		}
		return result;
	}

}
