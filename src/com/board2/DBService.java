package com.board2;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DBService {
	private static final String DRIVER = "com.mysql.jdbc.Driver";
	private static final String DATABASE = "board";
	private static final String USER = "root";
	private static final String PASSWORD = "1111";
	private static DBService instance = new DBService();
	

	private DBService()  {
		try {
			Class.forName(DRIVER);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static Connection getConnection() throws SQLException {
		return DriverManager.getConnection("jdbc:mysql://localhost:3306/" + DATABASE, USER, PASSWORD);
	}

	public static void disConnection(Connection conn, PreparedStatement preStmt, ResultSet rs) {
		try {
			if (rs != null) {
				rs.close();
				rs = null;
			}
			if (preStmt != null) {
				preStmt.close();
				preStmt = null;
			}
			if (conn != null) {
				conn.close();
				conn = null;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
