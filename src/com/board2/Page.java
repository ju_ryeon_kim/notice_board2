package com.board2;

public class Page {

	private int currentPage = 1;
	private int articlesPerPage = 10;
	private int totalArticleCount;
	private int maxPage;

	public enum ArticlesPerPage {
		TEN(10), TWENTY(20), FIFTY(50);
		private int num;

		private ArticlesPerPage(int num) {
			this.num = num;
		}
		public int getValue() {
			return num;
		}
	}

	public Page(int totalArticleCount) {
		this.totalArticleCount = totalArticleCount;
		setArticlesPerPage(articlesPerPage);
	}
	
	public Page(int totalArticleCount, int articlesPerPage) {
		this(totalArticleCount);
		setArticlesPerPage(articlesPerPage);
	}

	public int getPageOffset() {
		return articlesPerPage * (currentPage - 1);
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(int currentPage) {
		if (1 > currentPage) {
			throw new IllegalArgumentException("1보다 작은값 불가");
		}
		if (maxPage < currentPage) {
			throw new IllegalArgumentException("총페이지수보다 큰값 불가");
		}
		this.currentPage = currentPage;
	}

	public int getArticlesPerPage() {
		return articlesPerPage;
	}

	public void setArticlesPerPage(int articlesPerPage) {
		if (1 > articlesPerPage) {
			throw new IllegalArgumentException("1보다 작은값 불가");
		}

		this.articlesPerPage = articlesPerPage;
		setMaxPage();
	}

	public int getMaxPage() {
		return maxPage;
	}

	private void setMaxPage() {
		this.maxPage = (int) Math.ceil(totalArticleCount / (double) articlesPerPage);
	}
	
	public void setPage(int currentPage, int articlesPerPage, boolean changeArticlesPerPage) {
		if (1 > articlesPerPage) {
			articlesPerPage = 10;
		}
		setArticlesPerPage(articlesPerPage);
		if (1 > currentPage) {
			currentPage = 1;
		}
		if (getMaxPage() < currentPage) {
			currentPage = getMaxPage();
		}
		setCurrentPage(currentPage);
		if (changeArticlesPerPage) {
			setCurrentPage(1);
		}
	}

}
