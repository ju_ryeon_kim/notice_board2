package com.board2;

import java.util.ArrayList;
import java.util.List;

public class Board {
  public List<Article> articles = new ArrayList<>();
  private int articlesPerPage = 10;
  private int currentPage;
  
	public int getArticleCount() {
		return articles.size();
	}

	public void add(Article article) {
		if (0 == currentPage) {
			currentPage = 1;
		}
		articles.add(article);
	}

	public void setArticlesPerPage(int articlesPerPage) throws Exception {
		if (articlesPerPage < 1) {
			throw new IllegalArgumentException("1이상의 값만 가능");
		}
		this.articlesPerPage = articlesPerPage;
	}

	public int getArticlesPerPage() {
		return articlesPerPage;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(int currentPage) {
		if (1 > currentPage) {
			throw new IllegalArgumentException("1보다 작은 현재페이지는 불가");
		}
		
		if (Math.floor(articles.size() / articlesPerPage) < currentPage) {
			throw new IllegalArgumentException("전체 페이지수보다 큰 현재페이지는 불가");
		}
		this.currentPage = currentPage;
	}
	

}
